FROM python:3

ENV PYTHONUNBUFFERED=1

RUN mkdir /djangoapp

WORKDIR /djangoapp

ADD . /djangoapp

COPY ./requirements.txt /djangoapp/requirements.txt

RUN pip install -r requirements.txt

EXPOSE 8000

COPY . /djangoapp/

