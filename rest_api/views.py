from django.shortcuts import render
from django.http.response import HttpResponse
from django.views import View


class DevOp(View):

	def get_ip_address(self, request):
		x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
		if x_forwarded_for:
			ip_address = x_forwarded_for.split(',')[0]
		else:
			ip_address = request.META.get('REMOTE_ADDR')
		return HttpResponse(ip_address)

	def get(self,request):
		return HttpResponse('IP address returned')

# Create your views here.
